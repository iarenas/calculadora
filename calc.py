# Funcion suma con dos parametros
def suma(a, b):
    return a + b


# Funcion resta con dos parametros
def resta(a, b):
    return a - b


if __name__ == "__main__":

    # Suma = 1 + 2
    print(f"La suma de 1 y 2 es = {suma(1, 2)}")

    # Suma = 3 + 4
    print(f"La suma de 3 y 4 es = {suma(3, 4)}")

    # Resta = 5 - 6
    print(f"La resta de 5 de 6 es = {resta(5, 6)}")

    # Resta = 7 - 8
    print(f"La resta de 7 de 8 es = {resta(7, 8)}")